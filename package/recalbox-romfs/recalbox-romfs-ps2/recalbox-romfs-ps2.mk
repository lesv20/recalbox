################################################################################
#
# recalbox-romfs-ps2
#
################################################################################

# Package generated with :
# ./scripts/linux/empack.py --force --system ps2 --extension '.iso .bin .img .mdf .nrg' --fullname 'Sony Playstation 2' --platform ps2 --theme ps2 1:pcsx2:pcsx2:BR2_PACKAGE_PCSX2

# Name the 3 vars as the package requires
RECALBOX_ROMFS_PS2_SOURCE = 
RECALBOX_ROMFS_PS2_SITE = 
RECALBOX_ROMFS_PS2_INSTALL_STAGING = NO
# Set the system name
SYSTEM_NAME_PS2 = ps2
SYSTEM_XML_PS2 = $(@D)/$(SYSTEM_NAME_PS2).xml
# System rom path
SOURCE_ROMDIR_PS2 = $(RECALBOX_ROMFS_PS2_PKGDIR)/roms

# CONFIGGEN_STD_CMD is defined in recalbox-romfs, so take good care that
# variables are global across buildroot


ifneq ($(BR2_PACKAGE_PCSX2),)
define CONFIGURE_MAIN_PS2_START
	$(call RECALBOX_ROMFS_CALL_ADD_SYSTEM,$(SYSTEM_XML_PS2),Sony Playstation 2,$(SYSTEM_NAME_PS2),.iso .bin .img .mdf .nrg,ps2,ps2)
endef

ifneq ($(BR2_PACKAGE_PCSX2),)
define CONFIGURE_PS2_PCSX2_START
	$(call RECALBOX_ROMFS_CALL_START_EMULATOR,$(SYSTEM_XML_PS2),pcsx2)
endef
ifeq ($(BR2_PACKAGE_PCSX2),y)
define CONFIGURE_PS2_PCSX2_PCSX2_DEF
	$(call RECALBOX_ROMFS_CALL_ADD_CORE,$(SYSTEM_XML_PS2),pcsx2,1)
endef
endif

define CONFIGURE_PS2_PCSX2_END
	$(call RECALBOX_ROMFS_CALL_END_EMULATOR,$(SYSTEM_XML_PS2))
endef
endif



define CONFIGURE_MAIN_PS2_END
	$(call RECALBOX_ROMFS_CALL_END_SYSTEM,$(SYSTEM_XML_PS2),$(SOURCE_ROMDIR_PS2),$(@D))
endef
endif

define RECALBOX_ROMFS_PS2_CONFIGURE_CMDS
	$(CONFIGURE_MAIN_PS2_START)
	$(CONFIGURE_PS2_PCSX2_START)
	$(CONFIGURE_PS2_PCSX2_PCSX2_DEF)
	$(CONFIGURE_PS2_PCSX2_END)
	$(CONFIGURE_MAIN_PS2_END)
endef

$(eval $(generic-package))
